﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OCR_ForceLibrary.Model
{
    //Mimic of Salesforce Attachment object
    public class Attachment
    {
        public const String SObjectTypeName = "Attachment";

        public string Id { get; set; }
        public string Name { get; set; }
        public string Body { get; set; }
        public string ContentType { get; set; }
        public string ParentId { get; set; }
    }
}
