﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OCR_ForceLibrary.Model
{
    //Mimic the Salesforce custom oject OCR_IT_Indexed_File
    public class OCR_IT_Indexed_File
    {
        public const String SObjectTypeName = "ocr_it__OCR_IT_Indexed_File__c";

        public string Id { get; set; }
        public string Name { get; set; }
        public string ocr_it__Attachment_ID__c { get; set; }
        public string ocr_it__Attachment_Name__c { get; set; }
        public string ocr_it__Content_type__c { get; set; }
        public bool ocr_it__Is_Processed__c { get; set; }
        public string ocr_it__OCR_IT_Download_File_URL__c { get; set; }
        public string ocr_it__OCR_IT_Job_Status__c { get; set; }
    }
}
