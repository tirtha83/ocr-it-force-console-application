﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OCR_ForceLibrary.Model
{
    //Mimic the Salesforce custom object OCR_IT_Job_Status
    public class OCR_IT_Job_Status
    {
        public const String SObjectTypeName = "ocr_it__OCR_IT_Job_Status__c";

        public string Id { get; set; }
        public string Name { get; set; }
        public string ocr_it__Attachment_Content_Type__c { get; set; }
        public string ocr_it__Attachment_File_Length__c { get; set; }
        public string ocr_it__Attachment_ID__c { get; set; }
        public string ocr_it__Attachment_Original_Name__c { get; set; }
        public string ocr_it__Attachment_Parent_ID__c { get; set; }
        public string ocr_it__Job_Failure_Reason__c { get; set; }
        public string ocr_it__Job_Status__c { get; set; }
        public string ocr_it__OCR_IT_Job_URL__c { get; set; }
    }
}
