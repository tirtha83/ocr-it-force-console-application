﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace OCR_ForceLibrary.Model
{
    internal class Credentials
    {
        internal static string ConsumerKey { get; set; }
        internal static string ConsumerSecret { get; set; }
        internal static string Username { get; set; }
        internal static string Password { get; set; }
        internal static bool IsSandboxUser { get; set; }

        internal static void LoadCredentials(XDocument ocrAttachmentConverterXml)
        {
            try
            {
                if (!CommonUtility.GetCredentialLocallyStored(ocrAttachmentConverterXml))
                {
                    ConsumerKey = CryptoUtility.GetDecryptedData(CommonUtility.GetConsumerKey(ocrAttachmentConverterXml));
                    ConsumerSecret = CryptoUtility.GetDecryptedData(CommonUtility.GetConsumerSecret(ocrAttachmentConverterXml));
                    Username = CryptoUtility.GetDecryptedData(CommonUtility.GetUsername(ocrAttachmentConverterXml));
                    Password = CommonUtility.GetConsolidatedPassword(ocrAttachmentConverterXml);
                    IsSandboxUser = CommonUtility.GetIsSandboxUser(ocrAttachmentConverterXml);
                }
            }
            catch (Exception exception)
            {
                Logger.LogMessageToFile(exception.Message, exception.StackTrace);
            }
        }

        internal static void SetCredentials(string key, string secret, string uName, string password, string token,
                                          bool isSandBoxUser)
        {
            ConsumerKey = key;
            ConsumerSecret = secret;
            Username = uName;
            Password = password + token;
            IsSandboxUser = isSandBoxUser;
        }
        
    }
}
