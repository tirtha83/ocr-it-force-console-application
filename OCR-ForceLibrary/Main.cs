﻿using System;
using System.Threading.Tasks;
using System.Xml.Linq;
using OCR_ForceLibrary.AttachmentUtility;
using OCR_ForceLibrary.Model;
using Salesforce.Common;

namespace OCR_ForceLibrary
{
    public class Main
    {
        public void ProcessOCR(string OCRAttachmentConverterXml)
        {
            //string OCRAttachmentConverterXml = "<OCRAttachmentConverter><CredentialLocallyStored>true</CredentialLocallyStored><SFCredentials><ConsumerKey>3MVG9Y6d_Btp4xp4txeJvYQ22c1kUlqd5HSk.Hx6k4bL4lxfyqqc7nrPidI.x7dUsYzuUAPLb0jBsvtSVmlzQ</ConsumerKey><ConsumerSecret>1495348613488811736</ConsumerSecret><Username>tirtha83@gmail.com</Username><Password>Tirthankar#1010</Password><SecurityToken>PgTuapbUMaSIP0aMsHBnfslH</SecurityToken><IsSandboxUser>false</IsSandboxUser></SFCredentials><wsvKey>hN1w84PVdy6GBZoGdcFaZ0h02MmRyx0b</wsvKey><fileReplaced>true</fileReplaced><CleanupSettings><Deskew>true</Deskew><RemoveGarbage>true</RemoveGarbage><RemoveTexture>true</RemoveTexture><RotationType>Automatic</RotationType></CleanupSettings><OCRSettings><PrintType>Print</PrintType><OCRLanguage>English</OCRLanguage><SpeedOCR>false</SpeedOCR><AnalysisMode>MixedDocument</AnalysisMode><LookForBarcodes>true</LookForBarcodes></OCRSettings><OutputSettings><ExportFormat>PDF</ExportFormat></OutputSettings></OCRAttachmentConverter>";

            var task = DoOperation(OCRAttachmentConverterXml);
            task.Wait();
        }

        public void SetSalesforceCredentials(string consumerKey, string consumerSecret, string userName, string password,
                                             string securityToken, string isSandBoxUser)
        {
            Credentials.SetCredentials(consumerKey, consumerSecret, userName, password, securityToken, Convert.ToBoolean(isSandBoxUser));
        }

        private static async Task DoOperation(string ocrConverterXml)
        {
            try
            {
                if (string.IsNullOrEmpty(ocrConverterXml))
                {
                    throw new ArgumentNullException("ocrConverterXml");
                }
                //Parse the xml
                XDocument ocrAttachmentConverterXml = XDocument.Parse(ocrConverterXml);
                //load the credentials
                Credentials.LoadCredentials(ocrAttachmentConverterXml);
                //Instantiate the client
                var auth = new AuthenticationClient();

                // Authenticate with Salesforce
                //Console.WriteLine("Authenticating with Salesforce");
                var url = Credentials.IsSandboxUser
                    ? "https://test.salesforce.com/services/oauth2/token"
                    : "https://login.salesforce.com/services/oauth2/token";

                await auth.UsernamePasswordAsync(Credentials.ConsumerKey, Credentials.ConsumerSecret, Credentials.Username, Credentials.Password, url);

                //Console.WriteLine("Connected to Salesforce");

                //Pass to Status Manager class for further processing
                OCR_Job_Status_Manager statusManager = new OCR_Job_Status_Manager();
                var task = statusManager.ProcessJobStatusAsync(ocrAttachmentConverterXml, auth);
                task.Wait();

                //Disposing the auth client
                auth.Dispose();

                //Console.WriteLine("Done.");
                // Console.ReadLine();
            }
            catch (Exception exception)
            {
                Logger.LogMessageToFile(exception.Message, exception.StackTrace);
                //Console.WriteLine(exception.Message);
            }

        }
    }
}
