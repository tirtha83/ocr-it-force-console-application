﻿using System;

namespace OCR_ForceConsole
{
    //General Logging utiltiy for any exception
    public class Logger
    {
        public static object obj = new object();
        public static string GetTempPath()
        {
            string path = "";
            if (string.IsNullOrEmpty(CommonUtility.ReadSetting("LogFileLocation")))
            {
                path = Environment.GetEnvironmentVariable("TEMP");
                if (!path.EndsWith("\\")) path += "\\";
            }
            else
            {
                path = CommonUtility.ReadSetting("LogFileLocation");
                if (!path.EndsWith("\\")) path += "\\";
            }
           
            return path;
        }

        public static void LogMessageToFile(string msg)
        {
            lock (obj)
            {
                System.IO.StreamWriter sw = System.IO.File.AppendText(
                GetTempPath() + "OCR_IT_SF Log File.txt");
                try
                {
                    string logLine = String.Format(
                        "{0:G}: {1}.", DateTime.Now, msg);
                    sw.WriteLine(logLine);
                }
                finally
                {
                    sw.Close();
                }
            }
            
        }
    }
}
