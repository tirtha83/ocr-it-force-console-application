﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Linq;
using OCR_ForceConsole.Model;
using Salesforce.Common;
using Salesforce.Force;

namespace OCR_ForceConsole.AttachmentUtility
{
    public class OCR_Job_Status_Manager
    {
        private string jobStatusQuery =
            "select Id, Name, Attachment_Content_Type__c, Attachment_File_Length__c, Attachment_ID__c, Attachment_Original_Name__c, Attachment_Parent_ID__c, Job_Failure_Reason__c, Job_Status__c, OCR_IT_Job_URL__c from OCR_IT_Job_Status__c where Job_Status__c = 'Initiated'";

        private string attachmentQuery = "SELECT Id, Name, Body, BodyLength, ContentType, ParentId from Attachment WHERE Id IN (";

        private string ocrItJobRequestBodyFormat =
            "<Job><InputType>{0}</InputType><InputURL></InputURL>{1}<InputBlob type=\"binary\">{2}</InputBlob></Job>";

        private string ocrJobStatusRequestURIFormat = "http://api.ocr-it.com/private_ocr/v2-32iow897y3/submit?wsvKey={0}";

        public async Task ProcessJobStatusAsync(XDocument ocrAttachmentConverterXml, AuthenticationClient auth)
        {
            try
            {
                //Instantiate client
                var client = new ForceClient(auth.InstanceUrl, auth.AccessToken, auth.ApiVersion);

                Console.WriteLine("Quering Job Status Lists");

                var initiatedJobStatusList = new List<OCR_IT_Job_Status>();
                //Get the jobStatus results from Salesforce
                var results = await client.QueryAsync<OCR_IT_Job_Status>(jobStatusQuery);

                initiatedJobStatusList.AddRange(results.Records);
                Console.WriteLine("job status received ::" + initiatedJobStatusList.Count);

                //Queue the attachment data to OCR-IT system
                if (initiatedJobStatusList.Count > 0)
                {
                    string ocrItAdvancedSettingsXmlPart =
                        CommonUtility.GetAdavancedSettingsPart(ocrAttachmentConverterXml);

                    string attachmentIds = "";
                    Dictionary<string, OCR_IT_Job_Status> attachmentJobStatusMap =
                        new Dictionary<string, OCR_IT_Job_Status>();

                    //Building the attachmentIds list
                    foreach (OCR_IT_Job_Status ocrItJobStatus in initiatedJobStatusList)
                    {
                        if (!attachmentJobStatusMap.ContainsKey(ocrItJobStatus.Attachment_ID__c))
                        {
                            attachmentJobStatusMap.Add(ocrItJobStatus.Attachment_ID__c, ocrItJobStatus);
                        }
                        attachmentIds = attachmentIds + " '" + ocrItJobStatus.Attachment_ID__c + "' ,";
                    }
                    attachmentIds = attachmentIds.Substring(0, attachmentIds.Length - 1) + ')';
                    attachmentQuery = attachmentQuery + attachmentIds;

                    var resultAttachments = new List<Attachment>();

                    //Query Salesforce for detailed information of Salesforce
                    var attachmentResults = await client.QueryAsync<Attachment>(attachmentQuery);
                    resultAttachments.AddRange(attachmentResults.Records);

                    foreach (Attachment attachment in resultAttachments)
                    {
                        Console.WriteLine("Sending attachment to OCR");

                        //Get the blob64 data of attachment.
                        var blob64Data = await client.GetBlobDataAsync<string>(attachment.Body);

                        string finalRequestBody = string.Format(ocrItJobRequestBodyFormat,
                                                                attachment.ContentType.Substring(
                                                                    attachment.ContentType.IndexOf('/') + 1), ocrItAdvancedSettingsXmlPart,
                                                                    blob64Data);

                        //OPTIONAL- Save the attachment file to hard drive
                        /*byte[] bName = Convert.FromBase64String(blob64Data);
                        File.WriteAllBytes("D:\\SF ASP project\\Attachments" + "\\" + attachment.Name, bName);*/

                        //Post the file to OCR for job request
                        var ocrResponseJobUrl = await client.PostOcrJobRequestAsync<string>(
                                string.Format(ocrJobStatusRequestURIFormat,
                                              CommonUtility.GetWsvKey(ocrAttachmentConverterXml)),
                                finalRequestBody);

                        XDocument jobStatusResponseXml = XDocument.Parse(ocrResponseJobUrl);

                        Console.WriteLine("OCR Response:: " + ocrResponseJobUrl);

                        string jobURL = "";
                        if (jobStatusResponseXml.Element("JobStatus").Element("Status").Value == "Processing"
                            || jobStatusResponseXml.Element("JobStatus").Element("Status").Value == "Submitted")
                        {
                            jobURL = jobStatusResponseXml.Element("JobStatus").Element("JobURL").Value;
                        }

                        if (!string.IsNullOrEmpty(jobURL))
                        {
                            OCR_IT_Job_Status item = attachmentJobStatusMap[attachment.Id];
                            if (item != null)
                            {
                                //Update the job url information back in Salesforce
                                if (item.Attachment_ID__c == attachment.Id)
                                {
                                    item.Job_Status__c = "In Progress";
                                    item.OCR_IT_Job_URL__c = jobURL;
                                    Console.WriteLine("Updating job Status record.");

                                    var success = await client.UpdateAsync(OCR_IT_Job_Status.SObjectTypeName, item.Id,
                                                           new
                                                               {
                                                                   Job_Status__c = "In Progress",
                                                                   OCR_IT_Job_URL__c = jobURL
                                                               });
                                    if (!string.IsNullOrEmpty(success.Errors.ToString()))
                                    {
                                        Console.WriteLine("Failed to update record!");
                                        Logger.LogMessageToFile("Failed to update record!");
                                    }

                                    Console.WriteLine("Successfully updated the record.");

                                }
                            }
                        }
                    }

                    Console.WriteLine("OCR job status queueing part done");
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                Logger.LogMessageToFile(exception.Message);
            }
            finally
            {
                Console.WriteLine("Sleeping thread for 5 min");
                Thread.Sleep(300000);

                Console.WriteLine("Sleep completed. Calling process file job");

                OCR_Indexed_File_Process_Manager indexedFileProcessManager = new OCR_Indexed_File_Process_Manager();
                var task = indexedFileProcessManager.ProcessIndexedFileFromOcrAsync(ocrAttachmentConverterXml, auth);
                task.Wait();
            }
        }
    }
}
