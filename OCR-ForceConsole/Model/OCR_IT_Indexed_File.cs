﻿using System;

namespace OCR_ForceConsole.Model
{
    //Mimic the Salesforce custom oject OCR_IT_Indexed_File
    public class OCR_IT_Indexed_File
    {
        public const String SObjectTypeName = "OCR_IT_Indexed_File__c";

        public string Id { get; set; }
        public string Name { get; set; }
        public string Attachment_ID__c { get; set; }
        public string Attachment_Name__c { get; set; }
        public string Content_type__c { get; set; }
        public bool Is_Processed__c { get; set; }
        public string OCR_IT_Download_File_URL__c { get; set; }
        public string OCR_IT_Job_Status__c { get; set; }
    }
}
