﻿using System;
using System.Xml.Linq;

namespace OCR_ForceConsole.Model
{
    public class Credentials
    {
        public static string ConsumerKey { get; set; }
        public static string ConsumerSecret { get; set; }
        public static string Username { get; set; }
        public static string Password { get; set; }
        public static bool IsSandboxUser { get; set; }

        public static void LoadCredentials(XDocument ocrAttachmentConverterXml)
        {
            try
            {
                if (CommonUtility.GetCredentialLocallyStored(ocrAttachmentConverterXml))
                {
                    ConsumerKey = CommonUtility.ReadSetting("ConsumerKey");
                    ConsumerSecret = CommonUtility.ReadSetting("ConsumerSecret");
                    Username = CommonUtility.ReadSetting("Username");
                    Password = CommonUtility.ReadSetting("Password") + CommonUtility.ReadSetting("SecurityToken");
                    IsSandboxUser = Convert.ToBoolean(CommonUtility.ReadSetting("IsSandboxUser"));
                }
                else
                {
                    ConsumerKey = CryptoUtility.GetDecryptedData(CommonUtility.GetConsumerKey(ocrAttachmentConverterXml));
                    ConsumerSecret = CryptoUtility.GetDecryptedData(CommonUtility.GetConsumerSecret(ocrAttachmentConverterXml));
                    Username = CryptoUtility.GetDecryptedData(CommonUtility.GetUsername(ocrAttachmentConverterXml));
                    Password = CommonUtility.GetConsolidatedPassword(ocrAttachmentConverterXml);
                    IsSandboxUser = CommonUtility.GetIsSandboxUser(ocrAttachmentConverterXml);
                }
            }
            catch (Exception exception)
            {
                Logger.LogMessageToFile(exception.Message);
            }
        }
    }
}
