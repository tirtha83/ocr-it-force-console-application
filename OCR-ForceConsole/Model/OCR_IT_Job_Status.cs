﻿using System;

namespace OCR_ForceConsole.Model
{
    //Mimic the Salesforce custom object OCR_IT_Job_Status
    public class OCR_IT_Job_Status
    {
        public const String SObjectTypeName = "OCR_IT_Job_Status__c";

        public string Id { get; set; }
        public string Name { get; set; }
        public string Attachment_Content_Type__c { get; set; }
        public string Attachment_File_Length__c { get; set; }
        public string Attachment_ID__c { get; set; }
        public string Attachment_Original_Name__c { get; set; }
        public string Attachment_Parent_ID__c { get; set; }
        public string Job_Failure_Reason__c { get; set; }
        public string Job_Status__c { get; set; }
        public string OCR_IT_Job_URL__c { get; set; }
    }
}
