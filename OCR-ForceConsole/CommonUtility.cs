﻿using System;
using System.Configuration;
using System.Xml.Linq;

namespace OCR_ForceConsole
{
    public class CommonUtility
    {
        public static string ReadSetting(string key)
        {
            string result = "";
            try
            {
                var appSettings = ConfigurationManager.AppSettings;
                result = appSettings[key] ?? "";
                //Console.WriteLine(result);
            }
            catch (ConfigurationErrorsException)
            {
                Console.WriteLine("Error reading app settings");
                Logger.LogMessageToFile("Error reading app settings");
            }
            return result;
        }

        public static bool GetCredentialLocallyStored(XDocument ocrAttachmentConverterXml)
        {
            bool value = false;
            try
            {
                value = Convert.ToBoolean(ocrAttachmentConverterXml.Element("OCRAttachmentConverter")
                                                                   .Element("CredentialLocallyStored")
                                                                   .Value);
            }
            catch (Exception exception)
            {
                Logger.LogMessageToFile(exception.Message);
            }
            return value;
        }

        public static bool GetAttachmentReplacedOption(XDocument ocrAttachmentConverterXml)
        {
            bool value = false;
            try
            {
                value = Convert.ToBoolean(ocrAttachmentConverterXml.Element("OCRAttachmentConverter")
                                                                   .Element("fileReplaced")
                                                                   .Value);
            }
            catch (Exception exception)
            {
                Logger.LogMessageToFile(exception.Message);
            }
            return value;
        }

        public static string GetOutputFilePrefixName(XDocument ocrAttachmentConverterXml, string sourceExt)
        {
            string prefixValue = "";
            try
            {
                prefixValue = ocrAttachmentConverterXml.Element("OCRAttachmentConverter")
                                                  .Element("OutputFileNameFormat")
                                                  .Element("Prefix")
                                                  .Value;

                if (ocrAttachmentConverterXml.Element("OCRAttachmentConverter")
                                                               .Element("OutputFileNameFormat")
                                                               .Element("OCRDate").Value == "BeforePrefix")
                {
                    prefixValue = string.IsNullOrEmpty(prefixValue) ? System.DateTime.Now.ToString() : System.DateTime.Now.ToString() + "_" + prefixValue;
                }
                if (ocrAttachmentConverterXml.Element("OCRAttachmentConverter")
                                                               .Element("OutputFileNameFormat")
                                                               .Element("OCRDate").Value == "AfterPrefix")
                {
                    prefixValue = string.IsNullOrEmpty(prefixValue) ? System.DateTime.Now.ToString() : prefixValue + "_" + System.DateTime.Now.ToString();
                }
                if (ocrAttachmentConverterXml.Element("OCRAttachmentConverter")
                                                              .Element("OutputFileNameFormat")
                                                              .Element("SourceExt").Value == "BeforePrefix")
                {
                    prefixValue = string.IsNullOrEmpty(prefixValue) ? sourceExt : sourceExt + "_" + prefixValue;
                }
                if (ocrAttachmentConverterXml.Element("OCRAttachmentConverter")
                                                               .Element("OutputFileNameFormat")
                                                               .Element("SourceExt").Value == "AfterPrefix")
                {
                    prefixValue = string.IsNullOrEmpty(prefixValue) ? sourceExt : prefixValue + "_" + sourceExt;
                }     
            }
            catch (Exception exception)
            {
                Logger.LogMessageToFile(exception.Message);
            }
            return string.IsNullOrEmpty(prefixValue) ? prefixValue : prefixValue + "_";
        }

        public static string GetOutputFileSuffixName(XDocument ocrAttachmentConverterXml, string sourceExt)
        {
            string suffixValue = "";
            try
            {
                suffixValue = ocrAttachmentConverterXml.Element("OCRAttachmentConverter")
                                                 .Element("OutputFileNameFormat")
                                                 .Element("Suffix")
                                                 .Value;
                if (ocrAttachmentConverterXml.Element("OCRAttachmentConverter")
                                                               .Element("OutputFileNameFormat")
                                                               .Element("OCRDate").Value == "BeforeSuffix")
                {
                    suffixValue = string.IsNullOrEmpty(suffixValue) ? System.DateTime.Now.ToString() : System.DateTime.Now.ToString() + "_" + suffixValue;
                }
                if (ocrAttachmentConverterXml.Element("OCRAttachmentConverter")
                                                               .Element("OutputFileNameFormat")
                                                               .Element("OCRDate").Value == "AfterSuffix")
                {
                    suffixValue = string.IsNullOrEmpty(suffixValue) ? System.DateTime.Now.ToString() : suffixValue + "_" + System.DateTime.Now.ToString();
                }
                if (ocrAttachmentConverterXml.Element("OCRAttachmentConverter")
                                                              .Element("OutputFileNameFormat")
                                                              .Element("SourceExt").Value == "BeforeSuffix")
                {
                    suffixValue = string.IsNullOrEmpty(suffixValue) ? sourceExt : sourceExt + "_" + suffixValue;
                }
                if (ocrAttachmentConverterXml.Element("OCRAttachmentConverter")
                                                               .Element("OutputFileNameFormat")
                                                               .Element("SourceExt").Value == "AfterSuffix")
                {
                    suffixValue = string.IsNullOrEmpty(suffixValue) ? sourceExt : suffixValue + "_" + sourceExt;
                }     
            }
            catch (Exception exception)
            {
                Logger.LogMessageToFile(exception.Message);
            }
            return string.IsNullOrEmpty(suffixValue) ? suffixValue : suffixValue + "_";
        }

        public static string GetConsumerKey(XDocument ocrAttachmentConverterXml)
        {
            string value = "";
            try
            {
                value = ocrAttachmentConverterXml.Element("OCRAttachmentConverter")
                                                 .Element("SFCredentials")
                                                 .Element("ConsumerKey")
                                                 .Value;
            }
            catch (Exception exception)
            {
                Logger.LogMessageToFile(exception.Message);
            }
            return value;
        }

        public static string GetConsumerSecret(XDocument ocrAttachmentConverterXml)
        {
            string value = "";
            try
            {
                value = ocrAttachmentConverterXml.Element("OCRAttachmentConverter")
                                                 .Element("SFCredentials")
                                                 .Element("ConsumerSecret")
                                                 .Value;
            }
            catch (Exception exception)
            {
                Logger.LogMessageToFile(exception.Message);
            }
            return value;
        }

        public static string GetUsername(XDocument ocrAttachmentConverterXml)
        {
            string value = "";
            try
            {
                value = ocrAttachmentConverterXml.Element("OCRAttachmentConverter")
                                                 .Element("SFCredentials")
                                                 .Element("Username")
                                                 .Value;
            }
            catch (Exception exception)
            {
                Logger.LogMessageToFile(exception.Message);
            }
            return value;
        }

        public static string GetWsvKey(XDocument ocrAttachmentConverterXml)
        {
            string value = "";
            try
            {
                value = CryptoUtility.GetDecryptedData(ocrAttachmentConverterXml.Element("OCRAttachmentConverter")
                                                 .Element("wsvKey")
                                                 .Value);
            }
            catch (Exception exception)
            {
                Logger.LogMessageToFile(exception.Message);
            }
            return value;
        }

        public static string GetConsolidatedPassword(XDocument ocrAttachmentConverterXml)
        {
            string value = "";
            try
            {
                string password = CryptoUtility.GetDecryptedData(ocrAttachmentConverterXml.Element("OCRAttachmentConverter")
                                                           .Element("SFCredentials")
                                                           .Element("Password")
                                                           .Value);
                string SecurityToken = CryptoUtility.GetDecryptedData(ocrAttachmentConverterXml.Element("OCRAttachmentConverter")
                                                                .Element("SFCredentials")
                                                                .Element("SecurityToken")
                                                                .Value);
                value = password + SecurityToken;

            }
            catch (Exception exception)
            {
                Logger.LogMessageToFile(exception.Message);
            }
            return value;
        }

        public static bool GetIsSandboxUser(XDocument ocrAttachmentConverterXml)
        {
            bool value = false;
            try
            {
                value = Convert.ToBoolean(ocrAttachmentConverterXml.Element("OCRAttachmentConverter")
                                                                   .Element("SFCredentials")
                                                                   .Element("IsSandboxUser")
                                                                   .Value);
            }
            catch (Exception exception)
            {
                Logger.LogMessageToFile(exception.Message);
            }
            return value;
        }

        internal static string GetAdavancedSettingsPart(XDocument ocrAttachmentConverterXml)
        {
            string value = "";
            try
            {
                string cleanUpSettings =
                    ocrAttachmentConverterXml.Element("OCRAttachmentConverter")
                                             .Element("CleanupSettings")
                                             .ToString(SaveOptions.DisableFormatting);
                string OCRSettings = ocrAttachmentConverterXml.Element("OCRAttachmentConverter")
                                                              .Element("OCRSettings")
                                                              .ToString(SaveOptions.DisableFormatting);
                string OutputSettings = ocrAttachmentConverterXml.Element("OCRAttachmentConverter")
                                                                 .Element("OutputSettings")
                                                                 .ToString(SaveOptions.DisableFormatting);

                value = cleanUpSettings + OCRSettings + OutputSettings;

            }
            catch (Exception exception)
            {
                Logger.LogMessageToFile(exception.Message);
            }
            return value;
        }
    }
}
