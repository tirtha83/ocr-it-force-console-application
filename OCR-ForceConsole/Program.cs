﻿/*.Net client for Salesforce specially tailored for OCR-IT Attachment Converter.
 Developed on top of Force.com .Net connector.
 Developed By - Tirthankar 09th May 2015*/

using System;
using System.Xml.Linq;
using OCR_ForceLibrary;
using OCR_ForceConsole.AttachmentUtility;
using OCR_ForceConsole.Model;
using Salesforce.Common;
using System.Threading.Tasks;

namespace OCR_ForceConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                //string OCRAttachmentConverterXml = args[0];
                //Console.WriteLine(OCRAttachmentConverterXml);
                //Console.WriteLine(CryptoUtility.GetDecryptedData("VongQyBTt+94nBHmnM7gsz/RLXiAR8XiM1c+Fbptaks="));
                //Console.ReadLine();
                //Logger.LogMessageToFile(OCRAttachmentConverterXml);
                //string OCRAttachmentConverterXml = "<OCRAttachmentConverter><CredentialLocallyStored>false</CredentialLocallyStored><SFCredentials><ConsumerKey>pLRre6N/FWfpI84SaPge/jNtds1tGH+qrFUnfAUX2Db3uYzX80xjWaiJ8tWq7Mh9c9j8CSQp912hYHX+GeBxD6Y2+NEtoIl/XU4S8bveqy1OXNX0sW7b+GPb8oLekfYApuVAmmT+mh4dku47xVzbMA==</ConsumerKey><ConsumerSecret>cmA4idVNivwkyEfa6RkWpG0f4EZdah3NutbohHaDhQ2dRpkNjvewyYXeIK+yID3I</ConsumerSecret><Username>AfntDD51vLn0DVBpfqsulEs7DtfdyPZI012caURqerM4U9aaqIDOmj1wONe+Krfz</Username><Password>VongQyBTt+94nBHmnM7gsz/RLXiAR8XiM1c+Fbptaks=</Password><SecurityToken>cBBOmX1AXMN2V+maE1PPiHDSOJWB0KXDysZPwftm1uzFwDbl3JATbGMOZSlDHAI0</SecurityToken><IsSandboxUser>false</IsSandboxUser></SFCredentials><wsvKey>Y1a9kN8sDG4k1qr75uV4tpmWtPoIn8YnE7QlWc1p/2ZCJrbeHSmUxTsGisFqZZRd1N2uKDxhA6T42KEZuKuyEQ==</wsvKey><fileReplaced>true</fileReplaced><CleanupSettings><Deskew>true</Deskew><RemoveGarbage>true</RemoveGarbage><RemoveTexture>true</RemoveTexture><RotationType>Automatic</RotationType></CleanupSettings><OCRSettings><PrintType>Print</PrintType><OCRLanguage>Russian</OCRLanguage><SpeedOCR>false</SpeedOCR><AnalysisMode>MixedDocument</AnalysisMode><LookForBarcodes>true</LookForBarcodes></OCRSettings><OutputSettings><ExportFormat>PDF</ExportFormat></OutputSettings><OutputFileNameFormat><Prefix>PRE</Prefix><Suffix>SUF</Suffix><OCRDate>AfterPrefix</OCRDate><SourceExt>BeforeSuffix</SourceExt></OutputFileNameFormat></OCRAttachmentConverter>";
                
                //var task = DoOperation(OCRAttachmentConverterXml);
                //task.Wait();

                string OCRAttachmentConverterXml = "<OCRAttachmentConverter><CredentialLocallyStored>false</CredentialLocallyStored><SFCredentials><ConsumerKey>7hMZob2v77OVNNKMLkc9dSkGmjLSn9hbyeF44lpufQ9IBH/L92f/N/IpcivB7jbdPuZj63931vbxeFi6a3jyYlIIQP0bkruAP/YHr7MawTVSdDf3NgLnd+JSE9FFlelIMumya3X8vMiq3bvLi0gibw==</ConsumerKey><ConsumerSecret>UA+Ep6g1/qKyZ8DvxjDwmHrKhBteP0hPBSEb3aE/0mgdAWnPb6YSvJbMCwDqamNA</ConsumerSecret><Username>ARCw8LaMhjc6tuOv/hFTHYb8YcZcEaCKAaVEnYOTzeNgJSLwJEAyH4ymuQDI/eQ5</Username><Password>qrfkP6fMQj/qfTewBJs1yTwROWKz7RQXc1ztBDgxdgDnT9VIbMAdBLw3pSMCfgcy</Password><SecurityToken>InPDrU6Zh7usNR+BRroz7VKCrcIzCUwAEqZ61476Qk7sOwamo5nw7HjN5W9G+Rzj</SecurityToken><IsSandboxUser>false</IsSandboxUser></SFCredentials><wsvKey>acmBanUdPIWPwXKx61uPcSPaiai/k3Ql/3wRUqIlgh7i8g18HHCNiv+pWpyGuncnbVUY3/6TuMn7M1AbgbZt2g==</wsvKey><fileReplaced>false</fileReplaced><CleanupSettings><Deskew>true</Deskew><RemoveGarbage>true</RemoveGarbage><RemoveTexture>true</RemoveTexture><RotationType>Automatic</RotationType></CleanupSettings><OCRSettings><PrintType>Print</PrintType><OCRLanguage>English</OCRLanguage><SpeedOCR>false</SpeedOCR><AnalysisMode>MixedDocument</AnalysisMode><LookForBarcodes>true</LookForBarcodes></OCRSettings><OutputSettings><ExportFormat>PDF;Text</ExportFormat></OutputSettings><OutputFileNameFormat><Prefix>PRE</Prefix><Suffix></Suffix><OCRDate>BeforePrefix</OCRDate><SourceExt>None</SourceExt></OutputFileNameFormat></OCRAttachmentConverter>";
                OCR_ForceLibrary.Main main = new Main();
                main.ProcessOCR(OCRAttachmentConverterXml);

                //For the case where credential locally stored
                //string OCRConverterXmlWithLocallyStored = "<OCRAttachmentConverter><CredentialLocallyStored>true</CredentialLocallyStored><wsvKey>hN1w84PVdy6GBZoGdcFaZ0h02MmRyx0b</wsvKey><fileReplaced>true</fileReplaced><CleanupSettings><Deskew>true</Deskew><RemoveGarbage>false</RemoveGarbage><RemoveTexture>false</RemoveTexture><RotationType>Automatic</RotationType></CleanupSettings><OCRSettings><PrintType>Print</PrintType><OCRLanguage>English</OCRLanguage><SpeedOCR>false</SpeedOCR><AnalysisMode>MixedDocument</AnalysisMode><LookForBarcodes>false</LookForBarcodes></OCRSettings><OutputSettings><ExportFormat>PDF</ExportFormat></OutputSettings><OutputFileNameFormat><Prefix><Input>OCR</Input><OCRDate>true</OCRDate><SourceExt>false</SourceExt></Prefix><Suffix><Input>Demo</Input><OCRDate>false</OCRDate><SourceExt>true</SourceExt></Suffix></OutputFileNameFormat></OCRAttachmentConverter>";
                /*OCR_ForceLibrary.Main main = new Main();
                main.SetSalesforceCredentials("3MVG9Y6d_Btp4xp4txeJvYQ22c1kUlqd5HSk.Hx6k4bL4lxfyqqc7nrPidI.x7dUsYzuUAPLb0jBsvtSVmlzQ", "1495348613488811736", "tirtha83@gmail.com", "P@ssw0rd@83", "yNLBOYf1nthQcuzt3SOxgxsER", "false");
                main.ProcessOCR(OCRAttachmentConverterXml);*/
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                Logger.LogMessageToFile(e.Message + " :: " + e.StackTrace);

                var innerException = e.InnerException;
                while (innerException != null)
                {
                    Console.WriteLine(innerException.Message);
                    Console.WriteLine(innerException.StackTrace);

                    innerException = innerException.InnerException;
                }
            }
        }

        private static async Task DoOperation(string ocrConverterXml)
        {
            try
            {
                if (string.IsNullOrEmpty(ocrConverterXml))
                {
                    throw new ArgumentNullException("ocrConverterXml");
                }
                //Parse the xml
                XDocument ocrAttachmentConverterXml = XDocument.Parse(ocrConverterXml);
                //load the credentials
                Credentials.LoadCredentials(ocrAttachmentConverterXml);
                //Instantiate the client
                var auth = new AuthenticationClient();

                // Authenticate with Salesforce
                Console.WriteLine("Authenticating with Salesforce");
                var url = Credentials.IsSandboxUser
                    ? "https://test.salesforce.com/services/oauth2/token"
                    : "https://login.salesforce.com/services/oauth2/token";

                await auth.UsernamePasswordAsync(Credentials.ConsumerKey, Credentials.ConsumerSecret, Credentials.Username, Credentials.Password, url);

                Console.WriteLine("Connected to Salesforce");

                //Pass to Status Manager class for further processing
                OCR_Job_Status_Manager statusManager = new OCR_Job_Status_Manager();
                var task = statusManager.ProcessJobStatusAsync(ocrAttachmentConverterXml, auth);
                task.Wait();

                //Disposing the auth client
                auth.Dispose();

                Console.WriteLine("Done.");
                Console.ReadLine();
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                Logger.LogMessageToFile(exception.Message);
            }

        }
    }
}
