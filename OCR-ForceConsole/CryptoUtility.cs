﻿using System.Security.Cryptography;
using System.IO;
using System;

namespace OCR_ForceConsole
{
    internal class CryptoUtility
    {
        internal static string GetDecryptedData(string dataToDecrypt)
        {
            string plaintext;

            byte[] Key = Convert.FromBase64String("/5rAgGhOQUQ5vTCo0ru1XIzmaWDgwlPrhFGkX+VQDRw=");
            string encryptedbase64Password = dataToDecrypt;
            byte[] IV = new byte[16];
            byte[] phase = Convert.FromBase64String(encryptedbase64Password);
            Array.Copy(phase, 0, IV, 0, IV.Length);
            byte[] cipherText = new byte[phase.Length - 16]; ;
            Array.Copy(phase, 16, cipherText, 0, cipherText.Length);
            using (AesManaged aesAlg = new AesManaged())
            {
                aesAlg.Key = Key;
                aesAlg.IV = IV;
                // Create a decrytor to perform the stream transform.
                ICryptoTransform decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);
                // Create the streams used for decryption.
                using (MemoryStream msDecrypt = new MemoryStream(cipherText))
                {
                    using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader srDecrypt = new StreamReader(csDecrypt))
                        {
                            // Read the decrypted bytes from the decrypting stream
                            // and place them in a string.
                            plaintext = srDecrypt.ReadToEnd();
                        }
                    }
                }
            }
            return plaintext;
        }
    }
}
