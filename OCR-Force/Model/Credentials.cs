﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace OCR_Force.Model
{
    public class Credentials
    {
        public static string ConsumerKey { get; set; }
        public static string ConsumerSecret { get; set; }
        public static string Username { get; set; }
        public static string Password { get; set; }
        public static bool IsSandboxUser { get; set; }

        public static void LoadCredentials(XDocument ocrAttachmentConverterXml)
        {
            try
            {
                if (!CommonUtility.GetCredentialLocallyStored(ocrAttachmentConverterXml))
                {
                    ConsumerKey = CommonUtility.GetConsumerKey(ocrAttachmentConverterXml);
                    ConsumerSecret = CommonUtility.GetConsumerSecret(ocrAttachmentConverterXml);
                    Username = CommonUtility.GetUsername(ocrAttachmentConverterXml);
                    Password = CommonUtility.GetConsolidatedPassword(ocrAttachmentConverterXml);
                    IsSandboxUser = CommonUtility.GetIsSandboxUser(ocrAttachmentConverterXml);
                }
            }
            catch (Exception exception)
            {
                
            }
        }

        public static void SetCredentials(string key, string secret, string uName, string password, string token,
                                          bool isSandBoxUser)
        {
            ConsumerKey = key;
            ConsumerSecret = secret;
            Username = uName;
            Password = password + token;
            IsSandboxUser = isSandBoxUser;
        }
    }
}
