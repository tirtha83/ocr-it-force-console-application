﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Xml.Linq;
using OCR_Force.Model;
using Salesforce.Common;
using Salesforce.Force;

namespace OCR_Force.AttachmentUtility
{
    public class OCR_Indexed_File_Process_Manager
    {
        private string jobStatusQuery =
            "select Id, Name, Attachment_Content_Type__c, Attachment_File_Length__c, Attachment_ID__c, Attachment_Original_Name__c, Attachment_Parent_ID__c, Job_Failure_Reason__c, Job_Status__c, OCR_IT_Job_URL__c from OCR_IT_Job_Status__c where Job_Status__c = 'In Progress'";

        public async Task ProcessIndexedFileFromOcrAsync(XDocument ocrAttachmentConverterXml, AuthenticationClient auth)
        {
            try
            {
                var client = new ForceClient(auth.InstanceUrl, auth.AccessToken, auth.ApiVersion);

               // Console.WriteLine("Quering Job Status Lists of InProgress status");
                var inProgressJobStatusList = new List<OCR_IT_Job_Status>();

                var results = await client.QueryAsync<OCR_IT_Job_Status>(jobStatusQuery);

                inProgressJobStatusList.AddRange(results.Records);
                //Console.WriteLine("job status received ::" + inProgressJobStatusList.Count);

                //Queue the attachment data to OCR-IT system
                if (inProgressJobStatusList.Count > 0)
                {
                    bool isAttachmentReplaced = CommonUtility.GetAttachmentReplacedOption(ocrAttachmentConverterXml);

                    //Make Query
                    foreach (OCR_IT_Job_Status ocrItJobStatus in inProgressJobStatusList)
                    {
                        if (string.IsNullOrEmpty(ocrItJobStatus.OCR_IT_Job_URL__c))
                            continue;

                        var ocrIndexedFileResponse = await client.GetOcrJobStatusAsync<string>(ocrItJobStatus.OCR_IT_Job_URL__c);
                        //Sample Response: <JobStatus xmlns:i="http://www.w3.org/2001/XMLSchema-instance"><JobURL>http://api.ocr-it.com:8000/ocr/v3/getStatus/cc0bd2fb0841455b884a237b2a23f0c4</JobURL><Status>Finished</Status><Download><File><Uri>http://api.ocr-it.com:8000/ocr/v3/download/0b94918d56b9435a9d91d7db6b868404.TXT</Uri><OutputType>TXT</OutputType></File><File><Uri>http://api.ocr-it.com:8000/ocr/v3/download/0b94918d56b9435a9d91d7db6b868404.PDF</Uri><OutputType>PDF</OutputType></File></Download></JobStatus>

                        XDocument responseXDocument = XDocument.Parse(ocrIndexedFileResponse);

                        if (ocrItJobStatus.OCR_IT_Job_URL__c != responseXDocument.Element("JobStatus").Element("JobURL").Value)
                            continue;

                        switch (responseXDocument.Element("JobStatus").Element("Status").Value)
                        {
                            case "Finished":
                                Dictionary<string, string> downloadFileUrlOutputTypeMap = new Dictionary<string, string>();
                                foreach (XElement fileNode in responseXDocument.Element("JobStatus").Element("Download").Elements("File"))
                                {
                                    if (!downloadFileUrlOutputTypeMap.ContainsKey(fileNode.Element("Uri").Value))
                                    {
                                        downloadFileUrlOutputTypeMap.Add(fileNode.Element("Uri").Value, fileNode.Element("OutputType").Value);
                                    }
                                }

                                foreach (KeyValuePair<string, string> keyValuePair in downloadFileUrlOutputTypeMap)
                                {
                                    byte[] processedFileArray =
                                        await client.GetOcrIndexedFileAsync<byte[]>(keyValuePair.Key);

                                    //OPTIONAL - Save file to local file system
                                    //File.WriteAllBytes("D:\\SF ASP project\\Attachments" + "\\" + ocrItJobStatus.Attachment_Original_Name__c.Substring(0, ocrItJobStatus.Attachment_Original_Name__c.IndexOf('.')) + "." + keyValuePair.Value, processedFileArray);

                                    if (processedFileArray.Length > 25000000)
                                    {
                                        //Console.WriteLine("File too big to insert or update in salesforce");
                                        //Logger.LogMessageToFile("File too big to insert or update in salesforce - File Name: " + ocrItJobStatus.Attachment_Original_Name__c.Substring(0, ocrItJobStatus.Attachment_Original_Name__c.IndexOf('.')) + "." + keyValuePair.Value);
                                        continue;
                                    }
                                    string base64FileData = Convert.ToBase64String(processedFileArray);

                                    if (ocrItJobStatus.Attachment_Content_Type__c == keyValuePair.Value &&
                                        isAttachmentReplaced)
                                    {
                                        //Update attachment
                                        var success = await client.UpdateAsync(Attachment.SObjectTypeName, ocrItJobStatus.Attachment_ID__c,
                                                           new
                                                           {
                                                               Body = base64FileData
                                                           });
                                        if (!string.IsNullOrEmpty(success.Errors.ToString()))
                                        {
                                            //Console.WriteLine("Failed to update record!");
                                        }
                                        else
                                        {
                                            OCR_IT_Indexed_File indexedFile = new OCR_IT_Indexed_File();
                                            indexedFile.OCR_IT_Job_Status__c = ocrItJobStatus.Id;
                                            indexedFile.Attachment_ID__c = ocrItJobStatus.Attachment_ID__c;
                                            indexedFile.Attachment_Name__c = ocrItJobStatus.Attachment_Original_Name__c;
                                            indexedFile.OCR_IT_Download_File_URL__c = keyValuePair.Key;
                                            indexedFile.Content_type__c = keyValuePair.Value;
                                            indexedFile.Is_Processed__c = true;

                                            indexedFile.Id = await client.CreateAsync(OCR_IT_Indexed_File.SObjectTypeName, indexedFile);
                                        }
                                    }
                                    else
                                    {
                                        //Create new attachment
                                        Attachment newAttachment = new Attachment();
                                        newAttachment.ParentId = ocrItJobStatus.Attachment_Parent_ID__c;
                                        newAttachment.Name = CommonUtility.GetOutputFilePrefixName(ocrAttachmentConverterXml, ocrItJobStatus.Attachment_Original_Name__c.Substring(ocrItJobStatus.Attachment_Original_Name__c.IndexOf('.') + 1)) +
                                            ocrItJobStatus.Attachment_Original_Name__c.Substring(0, ocrItJobStatus.Attachment_Original_Name__c.IndexOf('.')) +"_"+
                                                             CommonUtility.GetOutputFileSuffixName(ocrAttachmentConverterXml, ocrItJobStatus.Attachment_Original_Name__c.Substring(ocrItJobStatus.Attachment_Original_Name__c.IndexOf('.') + 1)) +
                                                             keyValuePair.Value;
                                        newAttachment.Body = base64FileData;
                                        newAttachment.ContentType = GetContentType(keyValuePair.Value);

                                        newAttachment.Id =
                                            await client.CreateAsync(Attachment.SObjectTypeName, newAttachment);

                                        OCR_IT_Indexed_File indexedFile = new OCR_IT_Indexed_File();
                                        indexedFile.OCR_IT_Job_Status__c = ocrItJobStatus.Id;
                                        indexedFile.Attachment_ID__c = newAttachment.Id;
                                        indexedFile.Attachment_Name__c = newAttachment.Name;
                                        indexedFile.OCR_IT_Download_File_URL__c = keyValuePair.Key;
                                        indexedFile.Content_type__c = keyValuePair.Value;
                                        indexedFile.Is_Processed__c = true;

                                        indexedFile.Id = await client.CreateAsync(OCR_IT_Indexed_File.SObjectTypeName, indexedFile);
                                    }
                                }

                                //Update job status of the record.
                                var jobStatusFinishedSuccess = await client.UpdateAsync(OCR_IT_Job_Status.SObjectTypeName, ocrItJobStatus.Id,
                                                           new
                                                           {
                                                               Job_Status__c = "Completed"
                                                           });
                                if (!string.IsNullOrEmpty(jobStatusFinishedSuccess.Errors.ToString()))
                                {
                                    //Console.WriteLine("Failed to update test record!");
                                }

                                break;
                            case "Processing":
                                //Do Nothing.
                                break;
                            case "Expired":
                                ocrItJobStatus.Job_Status__c = "Error";
                                ocrItJobStatus.Job_Failure_Reason__c = "Job Url Expired.";
                                var jobStatusSuccess = await client.UpdateAsync(OCR_IT_Job_Status.SObjectTypeName, ocrItJobStatus.Id,
                                                           new
                                                           {
                                                               Job_Status__c = "Error",
                                                               Job_Failure_Reason__c = "Job Url Expired."
                                                           });
                                if (!string.IsNullOrEmpty(jobStatusSuccess.Errors.ToString()))
                                {
                                    //Console.WriteLine("Failed to update test record!");
                                }
                                break;
                            default:
                                string message = "Error with Status";
                                message = message + responseXDocument.Element("JobStatus").Element("Status");
                                foreach (XElement element in responseXDocument.Element("JobStatus").Element("Errors").Elements("Error"))
                                {
                                    message = message + " :: Error Code: " + element.Element("Code").Value +
                                              " :: Error Message: " + element.Element("Message").Value;
                                }

                                var jobStatusErrorUpdateSuccess = await client.UpdateAsync(OCR_IT_Job_Status.SObjectTypeName, ocrItJobStatus.Id,
                                                          new
                                                          {
                                                              Job_Status__c = "Error",
                                                              Job_Failure_Reason__c = message
                                                          });
                                if (!string.IsNullOrEmpty(jobStatusErrorUpdateSuccess.Errors.ToString()))
                                {
                                    //Console.WriteLine("Failed to update test record!");
                                }
                                break;
                        }
                    }

                    //Console.WriteLine("OCR file update part done");
                }
            }
            catch (Exception exception)
            {
                //Console.WriteLine(exception.Message);
                //Logger.LogMessageToFile(exception.Message);
            }
        }

        private string GetContentType(string extension)
        {
            string contentType = "";
            if (extension == "PDF")
                contentType = "application/pdf";
            if (extension == "TXT")
                contentType = "text/plain";
            if (extension == "XLS")
                contentType = "application/vnd.ms-excel";
            if (extension == "CSV")
                contentType = "application/vnd.ms-excel";
            if (extension == "XML")
                contentType = "text/xml";
            if (extension == "PPT")
                contentType = "application/vnd.ms-powerpoint";
            if (extension == "HTM")
                contentType = "text/html";
            return contentType;
        }
    }
}
